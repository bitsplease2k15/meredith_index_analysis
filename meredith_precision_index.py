#!/usr/bin/env python
import pandas as pd
import numpy as np
import os
import shutil
import time, fnmatch

#splits raw data & saves a files for each segment_audience
def split_audiences(fpath, os_type):   
    all_audience=None
    if ".xlsx" in fpath:
        all_audience = pd.read_excel(fpath)
    elif ".csv" in fpath:
        all_audience = pd.read_csv(fpath, sep=",")
    segments = all_audience['SEGMENT'].unique()
    segments_df_dict = {}
    
    for segment in segments:
        if segment not in segments_df_dict:
            segments_df_dict[segment] = all_audience[all_audience['SEGMENT'] == segment].copy()
    
    segments_meta_data = {}
    for segment, segment_df in segments_df_dict.items():
        current_dir = os.getcwd()
        segment_dir = os.path.join(current_dir, r'{}'.format(segment))
        shutil.rmtree(segment_dir, ignore_errors=True)
        os.makedirs(segment_dir)
        if os_type=="windows":
            segment_fpath = r"{}\{}{}csv".format(segment_dir, segment, ".")
        elif os_type=="mac":
            segment_fpath = segment_dir+"/"+segment+".csv"
        if segment_fpath not in segments_meta_data:
            segment_df.to_csv(segment_fpath, index=False, header=True, sep="\t", line_terminator="\n")
            segments_meta_data[segment] = segment_fpath
    
    return segments_meta_data


def wrangle_seg(seg_fpath, tc_brand_ref_list, pct_filter, view_filter, tc_br_filter, bs_filter, ls_filter):
    seg_df = pd.read_csv(seg_fpath, sep="\t")
    all_tc = set(seg_df['TOPCATEGORY'].unique())
    if bs_filter!=None:
        beauty_style_df = pd.read_csv(bs_filter)
        beauty_style_df = beauty_style_df[['TOPCATEGORY', 'CATEGORYNAME']].drop_duplicates()
        beauty_style_df['tc_cat'] = beauty_style_df['TOPCATEGORY']+"_"+beauty_style_df['CATEGORYNAME']
        beauty_style_tc_cat_set = set(beauty_style_df['tc_cat'].values)

        seg_df['tc_cat'] = seg_df['TOPCATEGORY']+"_"+seg_df['CATEGORYNAME']

        def wrangle_tc_cat(val):
            if val!=None:
                tc, cat = val.strip().split("_")
                if tc == "Beauty and Style" and val in beauty_style_tc_cat_set:
                    return 1
                elif tc!="Beauty and Style":
                    return 1
                else:
                    return 0
            else:
                return val



        seg_df['tc_cat_val'] = seg_df['tc_cat'].apply(lambda val: wrangle_tc_cat(val))

        seg_df = seg_df[seg_df['tc_cat_val']==1].copy()
        seg_df.drop(columns = ['tc_cat', 'tc_cat_val'], axis=1, inplace=True)
    
    if ls_filter=="yes":
        seg_df = seg_df[seg_df['TOPCATTYPE']=='LifeStyle']#step-3

    if tc_br_filter!=None:
        seg_df['topcat_brand'] = seg_df['TOPCATEGORY'].astype('str') + "_" + seg_df['BRAND'].astype('str')#step-5
        seg_df = seg_df[(seg_df.topcat_brand.isin(tc_brand_ref_list))]
    
    if view_filter!=None:
        seg_df = seg_df[seg_df['SEG_VIEWS'] > view_filter].copy()#step-6

    if pct_filter!=None:
        seg_df = seg_df[seg_df['SEG_PCT'] > pct_filter].copy()  # step-6
    return seg_df


def compute_revised_index(seg_df):
    seg_df['normal_seg_views'] = (np.sqrt(seg_df['SEG_VIEWS']))/(np.sqrt(seg_df['SEG_VIEWS']).mean())
    seg_df['precision_index'] = ((seg_df['SEG_INDEX'] - 100) * seg_df['normal_seg_views']) + 100
    seg_df['abs_index_diff'] = seg_df['precision_index'] - seg_df['SEG_INDEX']
    seg_df['abs_index_diff'] = seg_df['abs_index_diff'].apply(lambda val: abs(val))
    
    return seg_df


def save_output_files(res_df, segment_fpath, os_user):
    t = time.localtime()
    timestamp = time.strftime('%b-%d-%Y_%H-%M-%S', t)
    segment_dir, original_seg_file = os.path.split(segment_fpath)
    seg_name = original_seg_file.split('.')[0]
    output_filename = seg_name+"_output_indexes_"+timestamp+".csv"
    output_fpath=None
    if os_user=="mac":
        output_fpath = segment_dir+"/"+output_filename
    elif os_user=="windows":
        output_fpath = r"{}\{}".format(segment_dir, output_filename)
    
    res_df.to_csv(output_fpath, index=False, sep="\t", header=True, line_terminator="\n")
    return seg_name, output_fpath


def outcome_records(seg_df, topcat):
    tc_df = seg_df[seg_df['TOPCATEGORY'] == topcat].copy()
    tc_df['traffic_index_tuple'] = list(zip(tc_df.precision_index, tc_df.normal_seg_views))
    
    traffic_threshold = tc_df['normal_seg_views'].median()
    index_threshold = 100
    
    def quadrant_analysis(val):
        index, traffic = val[0], val[1]
        if index > index_threshold and traffic >= traffic_threshold:
            return "maintain"
        elif index >index_threshold and traffic < traffic_threshold:
            return "invest"
        elif index <= index_threshold and traffic < traffic_threshold:
            return "find_value"
        elif index <= index_threshold and traffic >= traffic_threshold:
            return "improve_efficiency"
        
    tc_df['brand_cat_strength'] = tc_df['traffic_index_tuple'].apply(lambda val: quadrant_analysis(val))
    
    cols_select = ['TOPCATEGORY', 'BRAND', 'CATEGORYNAME', 'brand_cat_strength']
    
    return tc_df[cols_select]


def get_brand_cat_outcomes(seg_df):
    
    brand_cat_df_list = []
    for tc in seg_df['TOPCATEGORY'].unique():
        brand_cat_df = outcome_records(seg_df, tc)
        brand_cat_df_list.append(brand_cat_df)

    all_brand_cat = pd.concat(brand_cat_df_list, ignore_index=True)
    seg_df = pd.merge(seg_df, all_brand_cat, how="inner", on=['TOPCATEGORY', 'BRAND', 'CATEGORYNAME'])
    return seg_df


def get_cat_outcomes(seg_df):
    
    def wrangle_cat_strength(val):
        if type(val)==list or type(val) == np.ndarray:
            if "maintain" in val:
                return "maintain"
            elif "invest" in val:
                return "invest"
            elif "find_value" in val:
                return "find_value"
            else:
                return "improve_efficiency"
        else: return val
    
    cat_type = seg_df.groupby(['CATEGORYNAME'], as_index=False).agg(
                                                  cat_strength = ("brand_cat_strength", pd.Series.mode))
    cat_type['cat_strength'] = cat_type['cat_strength'].apply(lambda val: wrangle_cat_strength(val))
    seg_df = pd.merge(seg_df, cat_type, how="inner", on="CATEGORYNAME")
    out_col_order = ['SEGMENT', 'BAS_SEGMENT', 'BRAND', 'TOPCATEGORY', 'CATEGORYNAME',
                     'SEG_INDEX', 'precision_index', 'abs_index_diff', 'brand_cat_strength', 'cat_strength',
                     'normal_seg_views','topcat_brand', 'TOPCATTYPE',
                     'SEG_VIEWS', 'SEG_CONTENT', 'SEG_TC_VIEWS', 'SEG_TC_CONTENT', 'SEG_PCT',
                     'BAS_VIEWS', 'BAS_CONTENT', 'BAS_TC_VIEWS', 'BAS_TC_CONTENT', 'BAS_PCT']
    
    return seg_df[out_col_order]


def audience_segmentation_analysis(meta_data, os_user, pct_filter, view_filter, tc_br_filter, bs_filter, ls_filter):

    topCategory_brand_list = None
    if tc_br_filter!=None:
        with open(tc_br_filter, "r") as f:
            topCategory_brand_list = f.read().split("\n")

    seg_index_meta={}
    for segment, segment_fpath in meta_data.items():
        seg_df = wrangle_seg(segment_fpath, topCategory_brand_list, pct_filter, view_filter, tc_br_filter, bs_filter, ls_filter)
        seg_index_df = compute_revised_index(seg_df)
        seg_index_df = get_brand_cat_outcomes(seg_index_df)
        seg_index_df = get_cat_outcomes(seg_index_df)
        seg_name, seg_index_path = save_output_files(seg_index_df, segment_fpath, os_user)
        if seg_name not in seg_index_meta:
            seg_index_meta[seg_name] = seg_index_path
    return seg_index_meta


def main():    
    all_audience_fpath = input("Enter the filepath for analysis (that contains all segments, should be an Excel file, containing a single sheet ONLY with the input data): ")
    
    os_user = input("Enter OS in use i.e. mac/windows: ")
    
    seg_pct_thresh = input("Do you want to drop within a certain SEG_PCT threshold? (enter no OR a value between 0 to 1): ")
    seg_pct_thresh = float(seg_pct_thresh) if seg_pct_thresh!="no" else seg_pct_thresh
        
    seg_views_thresh = input("Do you want to drop records within SEG_VIEWS threshold? (enter no OR enter a value > 0): ")
    seg_views_thresh = float(seg_views_thresh) if seg_views_thresh!="no" else seg_views_thresh
    
    bs_filter_fpath = input("Do you want to filter certain categories under Beauty and Style (enter no OR the reference file path to be used for filtration, type: CSV): ")
    
    lifestyle_filter = input("Do you want to drop records where TOPCATTYPE ! = Lifestyle (i.e. analyze lifestyle only) [enter yes/no]: ")
    
    tc_brand_fpath = input("Do you want to filter records using reference file containing topcat_brand combinations (enter no or reference filepath, type: txt): ")
        
    audience_meta_data = split_audiences(all_audience_fpath, os_user)
    print("\n")
    for segment, segment_fpath in audience_meta_data.items():
        print("Segment Name: {} | Segment FilePath: {} \n".format(segment, segment_fpath))
        
    seg_index_meta = audience_segmentation_analysis(audience_meta_data, os_user, seg_pct_thresh, seg_views_thresh, tc_brand_fpath,
                                                    bs_filter_fpath, lifestyle_filter)
    
    for seg, seg_index_fpath in seg_index_meta.items():
        print("Segment: {} | Segment Indexes Output FilePath: {} \n".format(seg, seg_index_fpath))


if __name__=="__main__":
    main()



