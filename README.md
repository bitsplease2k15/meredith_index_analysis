### Audience Profile Analysis:-
* This analysis was completed using LUSA5 (i.e loreal) data, you can reproduce this analysis for another dataset as long as the input schema to the data is **exactly** the same

### Setup/Installation
* Clone/download the repo/folder to a location of your choice
* Open the terminal (Mac)/command-prompt (Windows) and type the following command:
```shell
pip3 install -r requirements.txt
```
* If above command fails try:
```shell
pip install -r requirements.txt
```
* If you use anaconda--add anaconda to system PATH or open anaconda-prompt and try:
```shell
conda install -r requirements.txt
```

### Sunil System Start:-
* Open Terminal & run the following commands one-by-one
```shell
export PATH=$PATH:/Users/sthomas7/Library/Python/3.8/bin
cd ~/Desktop/meredith_index_analysis
jupyter-notebook
```

### Project Structure:-
* The directory **input_data** contains 3 files:--   
     1. **raw_input_data.xlsx** (the RAW data, should always be Excel file, should contain a single sheet only & have the same input-schema as defined below)
     2. **beauty_style_filter.csv** (use this IF you want to filter certain categories under Beauty & Style, should be a comma seperated CSV & have the same schema as defined below with Beauty and Style as the only TOPCATEGORY)
     3. **topcat_brand_ref_list.txt** (use this IF you want to filter certain topcat_brand combinations (i.e Beauty and Style_instyle etc)
* Run meredith_precision_index.ipynb OR meredith_precision_index.py--it creates a directory for each segment and each of those directory contains the "raw_data" for that segment only i.e. bold_creat.csv & OUTPUT/result w/ index for that segment i.e. const_dream_output_indexes_Oct-28-2021_16-36-49
* mirror-analysis-sunil/insights_ppt.ipynb: contains the graph based on Sunil's deck: i.e. PV Share, Index value histogram for certain Segment & CATEGORYNAME (edit last cell accordingly)
* index-research-viz/index-viz.ipynb: change the output filepath in 2nd CELL and then run. You'll use this to do EDA when you tweak/change the equation for precision_index

### Input Schema (column names ARE CASE-SENSITIVE):-
* **SEGMENT:** segment audience (i.e. subtle_creat, bold_creat etc)
* **BAS_SEGMENT:** baseline audience (i.e. MDP Onboarded Baseline etc)
* **BRAND:** mtax brand (i.e. msl, sol etc)
* **TOPCATEGORY:** mtax top-category (i.e. Health, Food and Cooking etc)
* **CATEGORYRNAME:** mtax category-name (i.e. home baking, women's health etc)
* **SEG_VIEWS:** total page views for a triplet of (BRAND, TOPCATEGORY, CATEGORYNAME) in the given segment audience 
* **SEG_CONTENT:** total content (i.e. no. of pages) for a triplet of (BRAND, TOPCATEGORY, CATEGORYNAME) in the given segment audience
* **SEG_TC_VIEWS:** total page views for a pair of (BRAND, TOPCATEGORY) in the given segment audience
* **SEG_TC_CONTENT:** total content (i.e. no. of pages) for a pair of (BRAND, TOPCATEGORY) in the given segment audience
* **SEG_PCT:** SEG_VIEWS/SEG_TC_CONTENT ==> total_views(BRAND, TOPCATEGORY, CATEGORYNAME) / total_views(BRAND, TOPCATEGORY, CATEGORYNAME) in the given segment audience
* **BAS_VIEWS:** total page views for a triplet of (BRAND, TOPCATEGORY, CATEGORYNAME) in the given baseline audience 
* **BAS_CONTENT:** total content (i.e. no. of pages) for a triplet of (BRAND, TOPCATEGORY, CATEGORYNAME) in the given baseline audience
* **BAS_TC_VIEWS:** total page views for a pair of (BRAND, TOPCATEGORY) in the given baseline audience
* **BAS_TC_CONTENT:** total content (i.e. no. of pages) for a pair of (BRAND, TOPCATEGORY) in the given baseline audience
* **BAS_PCT:** BAS_VIEWS/BAS_TC_CONTENT ==> total_views(BRAND, TOPCATEGORY, CATEGORYNAME) / total_views(BRAND, TOPCATEGORY, CATEGORYNAME) in the given baseline audience
* **SEG_INDEX:** (SEG_PCT/BAS_PCT)*100 ==> centered around 100 ==> if seg_index = 108 then seg_audience has **8% higher** interest in that content wrt baseline, similary if seg_index = 92, then seg_audience has **8% lower** interest in that content wrt baseline --> also known as **traffic_distribution_index**
* **TOPCATTYPE:** If TOPCATEGORY is in any of these ('Health', 'Home', 'Travel', 'Beauty and Style', 'Sports and Physical Fitness', 'Holidays and Entertaining', 'Families and Relationships', 'Hobbies and Leisure', 'Society, Culture and Religion', 'Entertainment and Media', 'Garden') then **LifeStyle** else **Other**

### Cardinalities:-
* TOPCATEGORY has a **one-to-many** relationship with CATEGORYNAME (topcat: Home, cat_list = 'bedroom', 'bedroom details', 'mattresses' etc)
* BRAND has a **many-to-many** relationship with TOPCATEGORY (brand: msl, topcat_list = 'Home', 'Garden' etc)
* BRAND has a **many to many** relationship with CATEGORYNAME (brand: msl, cat_list = 'bedroom', 'bedroom details', 'mattresses' etc)

### Output Schema
(SEGMENT, BAS_SEGMENT, BRAND, TOPCATEGORY, CATEGORYNAME, SEG_INDEX, **precision_index**, **abs_index_diff**, **brand_cat_strength**, **cat_strength**, **normal_seg_views**, **topcat_brand**, TOPCATTYPE, SEG_VIEWS, SEG_CONTENT, SEG_TC_VIEWS, SEG_TC_CONTENT, SEG_PCT, BAS_VIEWS, BAS_CONTENT, BAS_TC_VIEWS, BAS_TC_CONTENT, BAS_PCT)

* All "non-bold" column names are defined in the Input Schema & are also present in the output
* **normal_seg_views:** normalized SEG_VIEWS (read further for exact definition)
* **topcat_brand:** concatenate a pair of (TOPCATEGORYNAME, BRAND) using "_" ==> (Health, msl) --> Health_msl (create during a wrangling step) 
* **precision_index:** Meredith Precision Index (read further for exact definition) 
* **abs_index_diff:** absolute_value(precision_index - SEG_INDEX 
* **brand_cat_strength:** "strength" of a pair of (BRAND, CATEGORYNAME) within a given segment audience & top-category-- can take following values **only**: "maintain", "invest", "find_value", "improve_efficiency" (read further for exact definition)
* **cat_strength:** "strength" of a CATEGORYNAME **regardless/irrespective** of BRAND within a given segment audience-- can take following values **only**: "maintain", "invest", "find_value", "improve_efficiency" (read further for exact definition)

### Meredith Precisison Index
![eqn](./readme_imgs/index_shot.png){height=50 width=60}

* **normal_seg_views:-**   
     1. the **numerator** represents "normalized" SEG_VIEWS (we purposely did NOT USE log or cube-root for normalization, as that would make the distribution "too uniform"--**we still want outlier categories such as celebrity news under Entertainment and Media or christmas under Holidays and Entertaining to still retain their scale**
	 2. the **denominator** represents the "normalized mean" (i.e. take mean AFTER normalization
	 3. Finally we **divide** normalized SEG_VIEWS by it's normalized mean--so that our final **feature/KPI** is **centered** around the mean and is ready to be used
	 
* **precision_index:-** let's take an example
     1. Assume for some CATEGORYNAME, SEG_INDEX=112 ==> the given segment audience is **12% more likely (aka probability)** interested in this content wrt to baseline audience
	 2. Now SEG_INDEX-100 ==> gives us that **"interest factor"** i.e. 12 from above example
	 3. Besides the "interest factor" we need to take into account the distribution of SEG_VIEWS (aka **popularity of categories**), thus we multiply (SEG_INDEX-100) or 12 in this example by the normal_seg_views defined above--**this helps "adjust" the "interest factor" based on popularity of the category (celebrity news in general has more page-views compared to gardening in NYC etc)**
	 4. Finally we add 100 back, because **BOTH SEG_INDEX & precision_index need to be centered around 100**
	 
### Usecases (category classification)
![outcome](./readme_imgs/usecase.png)

* The code for the above algorithm is provided below (**lines 109 to 131 in meredith_precision_index.py**):--
```python
def outcome_records(seg_df, topcat):
    tc_df = seg_df[seg_df['TOPCATEGORY'] == topcat].copy()
    tc_df['traffic_index_tuple'] = list(zip(tc_df.precision_index, tc_df.normal_seg_views))
    
    traffic_threshold = tc_df['normal_seg_views'].median()#adjust THIS val based on new data (NOT RECOMMENDED)
    index_threshold = 100 #adjust THIS val based on new data (if your index is "centered" around 130--then this change this 130 & vice-versa)
    
    def quadrant_analysis(val):
        index, traffic = val[0], val[1]
        if index > index_threshold and traffic >= traffic_threshold:
            return "maintain"
        elif index >index_threshold and traffic < traffic_threshold:
            return "invest"
        elif index <= index_threshold and traffic < traffic_threshold:
            return "find_value"
        elif index <= index_threshold and traffic >= traffic_threshold:
            return "improve_efficiency"
        
    tc_df['brand_cat_strength'] = tc_df['traffic_index_tuple'].apply(lambda val: quadrant_analysis(val))
    cols_select = ['TOPCATEGORY', 'BRAND', 'CATEGORYNAME', 'brand_cat_strength']
    return tc_df[cols_select]
```

* As mentioned the above method is run for **all top-categories** within a given segment-audience & then for ALL segments (code for that part below, lines 134 to 143 in **meredith_precision_index.py**):--
```python
def get_brand_cat_outcomes(seg_df):
    brand_cat_df_list = []
    for tc in seg_df['TOPCATEGORY'].unique():
        brand_cat_df = outcome_records(seg_df, tc)
        brand_cat_df_list.append(brand_cat_df)

    all_brand_cat = pd.concat(brand_cat_df_list, ignore_index=True)
    seg_df = pd.merge(seg_df, all_brand_cat, how="inner", on=['TOPCATEGORY', 'BRAND', 'CATEGORYNAME'])
    return seg_df
```

* So far get_brand_cat_outcomes classifies a **pair of (BRAND, CATEGORYNAME)** into maintain, invest, find_value or improve_efficiency. As a next step, we want to **classify CATEGORYNAME irrespective of BRAND**, for that we group by CATEGORYNAME & take the **mode of the brand_cat_strength column** created previously (see image & code below)
![outcome2](./readme_imgs/usecase1.png)

```python
def get_cat_outcomes(seg_df):
    
    def wrangle_cat_strength(val):
        if type(val)==list or type(val) == np.ndarray:
            if "maintain" in val:
                return "maintain"
            elif "invest" in val:
                return "invest"
            elif "find_value" in val:
                return "find_value"
            else:
                return "improve_efficiency"
        else: return val
    
    cat_type = seg_df.groupby(['CATEGORYNAME'], as_index=False).agg(
                                                  cat_strength = ("brand_cat_strength", pd.Series.mode))
    cat_type['cat_strength'] = cat_type['cat_strength'].apply(lambda val: wrangle_cat_strength(val))
    seg_df = pd.merge(seg_df, cat_type, how="inner", on="CATEGORYNAME")
    out_col_order = ['SEGMENT', 'BAS_SEGMENT', 'BRAND', 'TOPCATEGORY', 'CATEGORYNAME',
                     'SEG_INDEX', 'precision_index', 'abs_index_diff', 'brand_cat_strength', 'cat_strength',
                     'normal_seg_views','topcat_brand', 'TOPCATTYPE',
                     'SEG_VIEWS', 'SEG_CONTENT', 'SEG_TC_VIEWS', 'SEG_TC_CONTENT', 'SEG_PCT',
                     'BAS_VIEWS', 'BAS_CONTENT', 'BAS_TC_VIEWS', 'BAS_TC_CONTENT', 'BAS_PCT']
    
    return seg_df[out_col_order]
```

* The script creates an **output file per segment audience** & get_brand_cat_outcomes() & get_cat_outcomes is run for ALL segments
* **Outcome Definitions**:--
     1. **maintain** ==> the given CATEGORYNAME or pair of (BRAND, CATEGORYNAME) has high traffic & high index, thus client should **maintain spending** on it
     2. **invest** ==> the given CATEGORYNAME or pair of (BRAND, CATEGORYNAME) has low traffic & high index, thus client should **invest** into it or Meredith should focus on it to bring more traffic as it has a higher "likelihood" to be viewed by seg_audience compared to baseline
     3. **find_value** ==> the given CATEGORYNAME or pair of (BRAND, CATEGORYNAME) has low traffic & low index, thus one might find a **"potential" diamond in the rough** aka this is a niche group and one might find **something interesting**
     4. **improve_efficiency** ==> the given CATEGORYNAME or pair of (BRAND, CATEGORYNAME) has high traffic & low index. From a numerical standpoint ==> **the precision index already adjusts for popularity aka page-views** ==> categories in this ARE POPULAR, **BUT they are also popular in the baseline audience too** ==> thus we get a lower precision_index value since it uses the SEG_INDEX, which takes into account baseline viewership